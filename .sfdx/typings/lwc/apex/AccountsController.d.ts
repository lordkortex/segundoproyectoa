declare module "@salesforce/apex/AccountsController.getAccounts" {
  export default function getAccounts(): Promise<any>;
}
declare module "@salesforce/apex/AccountsController.manualShareRead" {
  export default function manualShareRead(param: {recordId: any}): Promise<any>;
}
